// import React, { Component } from 'react'
// import { Text, View } from 'react-native'

// export default class activity extends Component {
//     render() {
//         return (
//             <View>
//                 <Text> textInComponent </Text>
//             </View>
//         )
//     }
// }
import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, StyleSheet, Image, TouchableOpacity, Dimensions, ActivityIndicator, Modal } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';


import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';

import LinearGradient from 'react-native-linear-gradient';



import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';

import { Icon } from 'react-native-elements';


const HEADER_MAX_HEIGHT = wp(66);
const HEADER_MIN_HEIGHT = wp(20);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;




let screenWidth = Dimensions.get('window').width;

// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class activity extends Component {
  constructor(props) {
    super(props);
    this.index2 = 0;

  }

  state = {
    zoomImage: false,
    ListPromotion: [],
    scrollY: new Animated.Value(0),
    pickerDisplayed: false,
    test: null,





  };




  //   test(){
  //       Alert.alert('in')
  //   }



  // renderItem(item) {
  //   return (

  //   );
  // }



  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)


    return {

      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", textAlign: "center", flex: 1, fontSize: 20, fontWeight: '500' }}>โปรโมชั่น</Text>),


      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-down' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
      },
    }
  };
  getNew() {
    const url = "http://203.113.11.167/api/news";
    console.log(url);
    axios.get(url)
      .then(result => {
        var data = result.data.slice(0, 5)
        this.setState({ ListNews: data })
        // alert(JSON.stringify (this.state.ListNew))
      })
      .catch(err => {
        alert(JSON.stringify(err));
      })
  }


  componentDidMount() {
    this.getNew();
    var that = this;
    let items = Array.apply(null, Array(4)).map((v, i) => {
      return { id: i, src: 'http://placehold.it/200x200?text=' + (i + 1) };
    });
    that.setState({
      dataSource: items,
    });
  }


  isLegitIndex1(index, length) {
    if (index < 0 || index >= length) return false;
    return true;
  }

  pagination2 = (velocity) => {
    let nextIndex;
    if (Platform.OS == "ios")
      nextIndex = velocity > 0 ? this.index2 + 1 : this.index2 - 1;
    else
      nextIndex = velocity < 0 ? this.index2 + 1 : this.index2 - 1;
    if (this.isLegitIndex1(nextIndex, this.state.ListPromotion.length)) {
      this.index2 = nextIndex;
    }
    this.flatlist2.scrollToIndex({ index: this.index2, animated: true });
  }






  renderItem(item) {

    // const { } = style;
    return (
      <View style={{ flex: 1, alignSelf: 'center' }}>

        <TouchableOpacity
          // underlayColor={false}
          onPress={() => {

            this.setState({
              test: { name: item.name, shortdetail: item.shortdetail, fulldetail: item.fulldetail, image: item.image, image_details: item.image_details },
              pickerDisplayed: true
            })
          }}>

          <View style={{ alignSelf: 'center', marginBottom: wp(4), marginTop: wp('2%') }} >

            <View style={{
              width: wp(45), height: wp(50), backgroundColor: '#FFF', borderRadius: 10, alignItems: 'center', flexDirection: 'column'
              , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,
            }}>

              <View style={{ width: wp(45), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                <Image source={{ uri: item.image }} style={{ width: wp('45%'), height: wp('36%'), }} />
              </View>

              <View style={{ flexDirection: 'row' }}>
                {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 8 }}>
                  <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('35'), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: wp('3%') }}>{item.name}</Text>
                  <View style={{ flexDirection: 'column', marginTop: 1 }}>
                    <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('35'), fontSize: wp('2.5%'), fontFamily: 'Prompt-Light' }}>{item.shortdetail}</Text>
                  </View>
                  {/* <View style={styles.lineStylew4} />
    
                    <Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                </View>
              </View>

            </View>
          </View>
        </TouchableOpacity>

      </View>
    );
  }


  modaldetail = () => {
    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });
    return (
      <LinearGradient colors={['#FFF', '#FFF']} style={styles.linearGradient}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <ScrollView style={{ flex: 1, }}
            showsVerticalScrollIndicator={false}
            style={styles.fill}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
            )}>

            <View style={{ marginTop: wp(12), marginLeft: wp(4), }}>
              <Ionicons
                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                name='ios-arrow-back'
                color='black'
                size={30}

                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), fontWeight: '500' }}>กิจกรรมอาสา</Text>

            </View>



            <Text style={{ marginTop: wp(10), marginLeft: wp(3), marginRight: wp(3), width: wp(95), fontFamily: 'Prompt-SemiBold', fontSize: 18, color: 'black' }}>
              {this.state.test.name_news}
            </Text>

            <View style={styles.cardNewsContainer}>
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={this.state.test.image_details}
                sliderWidth={wp('100%')}
                itemWidth={wp(70)}
                enableSnap={true}
                loop={true}
                lockScrollWhileSnapping={true}
                renderItem={({ item }) => {
                  return (
                    <View style={{ shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                      <TouchableOpacity onPress={() => { this.setState({ zoomImage: true }) }}>
                        <Image style={{ alignSelf: 'center', recideMode: 'contian', width: wp(70), height: wp(50), borderRadius: 10, marginTop: wp(5) }} source={{ uri: item.imagedetail }}
                          PlaceholderContent={<ActivityIndicator />} />
                      </TouchableOpacity>

                      <Modal visible={this.state.zoomImage} transparent={true} swipeDirection='down' swipeThreshold={50} onSwipeComplete={() => this.setState({ zoomImage: false })} >
                        <ImageViewer imageUrls={this.state.test.image_details.map((item, index) => { return { url: item.imagedetail } })} />
                        <Icon
                          underlayColor={false}
                          // raised
                          name='md-close'
                          type='ionicon'
                          color='#FFF'
                          size={wp(7)}
                          containerStyle={{ position: 'absolute', right: wp(10), top: wp(12), alignSelf: 'flex-end', marginRight: wp(-2) }}
                          onPress={() => this.setState({ zoomImage: false })}
                        />
                      </Modal>
                    </View>
                  );
                }}
              />
            </View>

            <View style={{ alignItems: 'center' }}>
              <Text style={{ marginTop: wp(5), marginLeft: wp(3.5), marginRight: wp(3), width: wp(90), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black', fontWeight: '400', fontSize: wp(4) }}>
                รายละเอียดโครงการ
                  </Text>
              <Text style={{ marginTop: wp(3), marginLeft: wp(3), marginRight: wp(3), width: wp(90), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>
                {this.state.test.detail}
              </Text>



              <View style={styles.lineStylew2}></View>

              <View style={{ marginTop: wp(3), width: wp(90), alignItems: 'center', justifyContent: "center", }}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(80), marginTop: wp(1) }}>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/1.png')} style={{ width: wp(14), height: wp(14), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>เจ้าของโครงการ</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>คุณxxxxxxxxxxxxxxxxxxx</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>ตำแหน่งxxxxxxxxxxx</Text>


                  </View>

                  <View style={styles.lineStyle3} ></View>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/2.png')} style={{ width: wp(14), height: wp(14), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>เปิดรับสมัคร</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>1 สค. - 31 ธค 62</Text>

                  </View>

                </View>

                <View style={styles.lineStylew2}></View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(80), marginTop: wp(4) }}>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/3.png')} style={{ width: wp(15), height: wp(15), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>ระยะเวลาปฏิบัตรงาน</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>12 - 13 กพ 63</Text>

                  </View>

                  <View style={styles.lineStyle3} ></View>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/4.png')} style={{ width: wp(15), height: wp(15), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>สถานที่ปฏิบัตรงาน</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>หมู่บ้าน ปางเคาะ อ.เด่นชั่น จ.แพร่</Text>

                  </View>

                </View>

                <View style={styles.lineStylew2}></View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(80), marginTop: wp(4) }}>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/5.png')} style={{ width: wp(15), height: wp(15), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>จำนวนอาสาสมัตร</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>3 คน</Text>

                  </View>

                  <View style={styles.lineStyle3} ></View>

                  <View style={{ alignItems: 'center', width: wp(40), }}>
                    <Image source={require('../assets/component/กิจกรรมอาสา2/6.png')} style={{ width: wp(15), height: wp(15), resizeMode: 'contain' }} />
                    <Text style={{ marginTop: wp(2), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3.5) }}>สิ่งที่จัดเตรียมไว้ให้</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>- อาหาร</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>- ที่พัก</Text>
                    <Text style={{ marginTop: wp(1), fontFamily: 'Prompt-Light', color: 'grey', fontSize: wp(3) }}>- ค่าเดินทาง</Text>

                  </View>

                </View>


              <TouchableOpacity
                onPress={() => { }}
              // style={styles.login}

              >
                {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                <LinearGradient style={styles.registerASA} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#F5D540', '#F5D540']}>
                  <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#000', }}>สมัครครูอาสา</Text>
                </LinearGradient>
              </TouchableOpacity>



              </View>


            </View>


          </ScrollView>


          {/* <Animated.View style={[styles.header, , { height: headerHeight }]}>
              <Animated.View>
                <View style={styles.bar}>
                  <Text style={styles.title}  >TOT Easy Life</Text>
                </View>
              </Animated.View>
  
              <Animated.Image
                style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
                source={{ uri: this.state.test.image }} />
  
  
              <View style={{ marginTop: wp(-10), marginLeft: wp(4) }}>
                <Ionicons
                  containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                  name='ios-arrow-back'
                  color='white'
                  size={45}
  
                  onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
              </View>
            </Animated.View> */}



        </View>
      </LinearGradient>
    )
  }


  render() {
    return (
      <View >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
          <Text style={{ marginLeft: wp('5%'), flexDirection: 'row', alignSelf: 'flex-end', fontWeight: '500', fontSize: wp('4%'), fontFamily: 'Prompt-Light', marginTop: wp(2), }} >กิจกรรมอาสา</Text>




          {/* <TouchableOpacity onPress={() => this.props.route.navigate('seeallnewspage')} >
          <Image style={{ marginRight: wp('2%'), width: wp('8%'), height: wp('2%'), marginTop: wp('10%') }} source={require('../../ICON_Png/กรอบเท่ากัน/icondesign-10.png')} />
          <View style={{ flexDirection: 'row', marginRight: wp('5.5%'),marginTop:wp(3) }} >
            <Text style={{ fontFamily: 'prompt-light', fontWeight: '400', fontSize: wp(4), marginTop: wp('-2.3%'), }} >ดูเพิ่มเติม </Text>
          
            <Icon  name="md-more" size={25}  />

            <Text style={{ marginRight: wp('2%'), fontFamily: 'prompt-light', fontWeight: '500', fontSize: wp(4), marginTop: wp('-2.3%'), color: '#3d90ff' }} >></Text>
          </View>
        </TouchableOpacity>
        <Text style={{  marginTop: 10,fontWeight: 'bold', color: 'black' }}  onPress={() => console.log('2st')} > ></Text> */}

        </View>





        <View style={{ marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1, width: wp(90), alignSelf: 'center', alignContent: 'center' }}>
          <FlatList
            // style={{margin:wp(1.5)}}
            style={{}}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={this.state.ListNews}
            ref={ref => (this.flatlist2 = ref)}
            //onScrollEndDrag={e => {
            //this.pagination2(e.nativeEvent.velocity.x);
            //}}
            renderItem={({ item }) => {
              return (

                <TouchableOpacity
                  // underlayColor={false}
                  onPress={() => {
                    this.setState({
                      test: { name_news: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
                      pickerDisplayed: true
                    })
                  }}
                  style={styles.BlockNews}>
                  <View >
                    <View style={{ width: wp(88), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                      <Image source={{ uri: item.image }} style={{ width: wp('90%'), height: wp('45%'), }} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp('3%'), marginTop: wp('2%') }}>
                        <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('80'), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: wp('4%') }}>{item.name_news}</Text>
                        <View style={{ flexDirection: 'column', marginTop: 5 }}>
                          <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('80'), fontSize: wp('3%'), fontFamily: 'Prompt-Light' }}>{item.detail}</Text>
                        </View>

                        <View style={styles.lineStylew5} />
                        {/*                        
                        <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginRight: wp('4%'),  }}>
                        
                        <TouchableOpacity  style={{marginTop:wp('1%')}} onPress={() => { this.share() }}>
                          <Icon
                            // name={this.state.heartIcon}
                           
                            name='ios-share-alt'
                            type='ionicon'
                            size={18}
                            style={{ justifyContent: 'center',  }}
                            color='black'
                          />
                          <Text style={{ marginLeft: wp('5%'), fontFamily: 'Prompt-Light', fontSize: wp('3'), marginTop: wp('-4.5%') , }} >แชร์</Text>
                          </TouchableOpacity>

                        </View> */}

                        {/* <Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                      </View>

                      {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}


                    </View>

                  </View>

                </TouchableOpacity>
              )
            }}
          />

          {this.state.test != null ?
            <Modal

              animationType={"slide"}
              isVisible={this.state.pickerDisplayed}
              onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
              // swipeDirection={['down']}
              style={styles.bottomModal}
              onBackdropPress={() => this.setState({ pickerDisplayed: false })}

            >

              {
                this.modaldetail()
              }

            </Modal> : null
          }

        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  BlockNews: {
    flex: 1,
    backgroundColor: '#FFF',
    marginRight: wp(3),
    marginLeft: wp(1),
    borderRadius: 10,
    marginBottom: wp(3),
    marginTop: wp(3),
    width: wp(88),
    height: wp('63%'),
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 2, }, shadowOpacity: 0.3, shadowRadius: 2.65, elevation: 10,
    alignSelf: 'center', alignItems: 'center'
  },
  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },
  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',

    height: wp(60),
    width: wp('100')
  },

  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,

  },

  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  fill: {
    flex: 1,
  },
  bar: {
    marginTop: wp(8),
    height: wp(8),
    alignItems: 'center',
    justifyContent: 'center',

  },
  title: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: wp('4.5%'), fontWeight: '500',
    fontFamily: 'prompt-light'

  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000',
    overflow: 'hidden',
  },


  cardNewsContainer: {
    alignSelf: 'center',
    marginTop: wp(5),
    shadowColor: "#000",
    backgroundColor: 'rgb(238,238,238)',
    width: wp(100), height: wp(60),

  },


  BlockNews1: {
    flex: 1,
    backgroundColor: '#FFF',
    margin: wp(1.5),
    borderRadius: 10,
    shadowColor: "#000",
    marginBottom: wp(3), marginTop: wp(3),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
    alignSelf: 'center'
  },
  lineStylew1: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(100),
    height: wp(3),
    marginTop: wp(10),
    backgroundColor: 'rgb(239, 239, 239)'
  },
  lineStylew2: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(90),
    height: wp(0.5),
    marginTop: wp(4),
    backgroundColor: 'rgb(239, 239, 239)'
  },
  lineStyle3: {
    width: wp(0.4),
    height: wp(31),
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    backgroundColor: 'rgb(239, 239, 239)'
  },

  registerASA: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
    height: wp(10),
    width: wp(75),
    marginBottom:wp(10),
    marginTop: wp(10),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 30,
    // borderColor: '#fff'
  },
})