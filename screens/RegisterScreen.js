import React, { Component } from 'react';
import { Alert, View, Text, Button, StyleSheet, ScrollView, Image, TouchableOpacity, TextInput, Picker, AsyncStorage } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select'
import Axios from "axios";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DatePicker from 'react-native-datepicker'
import { color } from 'react-native-reanimated';

// const ProfileScreen = () => {
//   const [name, setName] = useState('');
//   const [IdCard, setIdCard] = useState('');

// const placeholder = {
//   gender: {
//     label: 'กรุณาเลือกเพศ',
//     value: null,
//     color: '#8e8e8e',
//   },


// };


export default class RegisterScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: '5',
      phoneNumber: '',
      getprovince: [],
      getdistrict: [],
      getsubdistrict: [],
      getvillage: [],
      IdCard: '',
      name: '',
      email: '',
      educational: '',
      occupation: '',
      interested: '',
      specialSkill: '',
      Name: '',
      Nationalid: '',
      dob: '',
      Age: '',
      Mobileno: '',
      email: '',
      gender: '',
      province: '',
      district: '',
      subdistrict: '',
      Address: '',


    };


  }


  addUser = () => {
    if (this.state.Name.length == '') {
      Alert.alert('กรุณากรอกชื่อจริงของท่าน',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    } else if (this.state.Nationalid.length == '') {
      Alert.alert('กรุณากรอกบัตรประชาชน',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    } else if (this.state.Nationalid.length < 13) {
      Alert.alert('กรุณากรอกบัตรประชาชนให้ครบ 13 หลัก',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    }
    else if (this.state.Age.length == '') {
      Alert.alert('กรุณากรอกอายุ',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    }

    else if (this.state.Mobileno.length == '') {
      Alert.alert('กรุณากรอกเบอร์ติดต่อกลับ',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    }

    else if (this.state.Address.length == '') {
      Alert.alert('กรุณากรอกที่อยู่',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])
    }
    else if (this.state.Mobileno.length < 10) {
      Alert.alert('กรุณากรอกเบอร์โทรให้ครบ 10 หลัก',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])

    } else if (this.state.email.length == '') {
      Alert.alert('กรุณากรอกอีเมลล์',
        "",
        [
          { text: 'ตกลง', cancelable: false },
        ])

    }


    else {
      const url = "http://203.113.14.29:3000/api/addusers";
      // var Nationalid64 = base64.encode(this.state.Nationalid);
      // var Email64 = base64.encode(this.state.Email);
      // var Passwordbase64 = base64.encode(this.state.Password);
      // var Faceid64 = base64.encode(this.state.Faceid);
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'JWT fefege...'
      }
      axios.post(url, {
        // ipaddress: this.state.Ipaddress,
        // auth_contracts: "easy2ife2ew",
        // auth_code: "6sd4f6@6cv4",
        // lang: "TH",
        name: this.state.Name,
        // lastname: this.state.Lastname,
        IDcard: this.state.Nationalid,
        age: this.state.Age,
        tel: this.state.Mobileno,
        Email: this.state.email,
        gender: this.state.male,
        gender: this.state.female,
        address: this.state.Address,
        province: this.state.province,
        district: this.state.district,
        subdistrict: this.state.subdistrict,
        Specialty: this.state.interested,
        Specialabilit: this.state.specialSkill
        //  faceid: base64.encode(Faceid64),
        // birthdate: this.state.Birthdate
      },
        {
          headers: headers
        }
      ).then(result => {
        console.log(result);

        if (result.status == 200) {
          // alert(JSON.stringify(result.data.statuscode))
          // this.setState({ mCode: result.data.result.mCode })
          // this.setState({ mConfirmAppCode: result.data.result.mConfirmAppCode })
          if (result.data.message == 'มีข้อมูลข้อมูลอยู่แล้ว') {
            Alert.alert(
              "สมัครสมาชิกไม่สำเร็จ",
              "เลขบัตรนี้มีผู้ใช้อยู่ในระบบแล้ว",
              [
                { text: 'ตกลง' },
              ],
              { cancelable: false },
            );
            console.log(result.data.data.IDcard);
          } else {
            Alert.alert(
              "สมัครสมาชิกสำเร็จ",
              "",
              [
                { text: 'ตกลง', onPress: () => (RNRestart.Restart()) },
              ],
              { cancelable: false },
            );
          }
          // Alert.alert(
          //     "สมัครสมาชิคสำเร็จ",
          //     "",
          //     [
          //         { text: 'ตกลง', onPress: () => (RNRestart.Restart()) },
          //     ],
          //     { cancelable: false },
          // );
          // console.log(result.data.data.IDcard);

          // this.props.navigation.navigate('Home')
        }
        // else {
        //     // var str = JSON.stringify(result.data.errors[0].msg._th)
        //     Alert.alert(
        //         "สมัครไม่สำเร็จ",
        //         "",
        //         [
        //             { text: 'ตกลง' },
        //         ],
        //         { cancelable: false },
        //     );
        // }
      }).catch(e => {
        console.log(e);

      });
    }
  }



  getDataProvince = () => {
    Axios.get("http://npcrapi.netpracharat.com/api/getallprovince").then(
      data => {
        // alert(JSON.stringify(data.data.data))
        this.setState({
          getprovince: data.data.data.map((i, index) => {
            return { value: i.name_th, label: i.name_th };
          })
        });
        if (this.state.province != null) {
          Axios.get(
            "http://npcrapi.netpracharat.com/api/getallprovince2?type=province&province=" +
            this.state.province
          ).then(data => {
            // alert(JSON.stringify(data.data) + "id:" + item2);
            this.setState({
              getdistrict: data.data.data.map((i, index) => {
                return { value: i.name_th, label: i.name_th };
              })
            });
            if (this.state.district != null) {
              Axios.get(
                "http://npcrapi.netpracharat.com/api/getallprovince2?type=district&province=" +
                this.state.province +
                "&district=" +
                this.state.district
              ).then(data => {
                // alert(JSON.stringify(data.data) + "id:" + item2);
                this.setState({
                  getsubdistrict: data.data.data.map((i, index) => {
                    return { value: i.name_th, label: i.name_th };
                  })
                });
                if (this.state.subdistrict != null) {
                  Axios.get(
                    "http://npcrapi.netpracharat.com/api/checkprovince?province=" +
                    this.state.province +
                    "&district=" +
                    this.state.district +
                    "&subdistrict=" +
                    this.state.subdistrict
                  ).then(data => {
                    // alert(data.data.data.length)
                    let dataCount = data.data.data.length;
                    this.villageCount = dataCount;
                    // alert(dataCount)
                    let village = data.data.data.concat({ village: "à¸­à¸·à¹ˆà¸™à¹†" });
                    // this.setState({ village: "à¸­à¸·à¹ˆà¸™à¹†" });
                    if (dataCount != 0) {
                      this.setState({
                        // loading: false,
                        getvillage: village.map((i, index) => {
                          return { value: i.village, label: i.village };
                        })
                      });
                    } else {
                      this.setState({ loading: false, village: "à¸­à¸·à¹ˆà¸™à¹†" });
                    }

                    Axios.get(
                      "http://npcrapi.netpracharat.com/api/checkprovince2?province=" +
                      this.state.province +
                      "&district=" +
                      this.state.district +
                      "&subdistrict=" +
                      this.state.subdistrict +
                      "&village=" +
                      this.state.village
                    ).then(data => {
                      // alert(data.data.data.length)
                      let dataCount = data.data.data.length;
                      // this.villageCount = dataCount;
                      // alert(dataCount)
                      let village = data.data.data.concat({ village: "à¸­à¸·à¹ˆà¸™à¹†" });
                      // this.setState({ village: "à¸­à¸·à¹ˆà¸™à¹†" });
                      if (dataCount != 0) {
                        this.setState({
                          loading: false,
                          getvillage: village.map((i, index) => {
                            return { value: i.village, label: i.village };
                          })
                        });
                      } else {
                        this.setState({ loading: false, village: "à¸­à¸·à¹ˆà¸™à¹†" });
                      }
                    });
                  });
                } else {
                  this.setState({
                    loading: false
                  });
                }
              });
            } else {
              this.setState({
                loading: false
              });
            }
          });
        } else {
          this.setState({
            loading: false
          });
        }
      }
    );
  };

  getData2(item1, item2, index) {
    if (item1 == "province") {
      this.setState({ province: item2 });
      Axios.get(
        "http://npcrapi.netpracharat.com/api/getallprovince2?type=province&province=" +
        item2
      ).then(data => {
        // alert(JSON.stringify(data.data) + "id:" + item2);
        this.setState({
          district: "",
          subdistrict: "",
          village: "",
          getdistrict: data.data.data.map((i, index) => {
            return { value: i.name_th, label: i.name_th };
          })
        });
      });
    } else if (item1 == "district") {
      this.setState({ district: item2 });
      // alert(item2)
      // this.setState({ district:this.state.getdistrict[index].name_th,})
      Axios.get(
        "http://npcrapi.netpracharat.com/api/getallprovince2?type=district&province=" +
        this.state.province +
        "&district=" +
        item2
      ).then(data => {
        // alert(JSON.stringify(data.data) + "id:" + item2);
        this.setState({
          subdistrict: "",
          village: "",
          getsubdistrict: data.data.data.map((i, index) => {
            return { value: i.name_th, label: i.name_th };
          })
        });
      });
    } else if (item1 == "sub_district") {
      this.setState({
        subdistrict: item2
      });
      // alert( JSON.stringify(this.state.getsubdistrict))
      // this.setState({subdistrict: this.state.getsubdistrict[index].name_th})
      Axios.get(
        "http://npcrapi.netpracharat.com/api/checkprovince?province=" +
        this.state.province +
        "&district=" +
        this.state.district +
        "&subdistrict=" +
        item2
      ).then(async data => {
        // alert(data.data.data.length)
        let dataCount = data.data.data.length;
        this.villageCount = dataCount;
        // alert(dataCount)
        let village = await data.data.data.concat({ village: "à¸­à¸·à¹ˆà¸™à¹†" });
        this.setState({ village: "" });
        if (dataCount != 0) {
          this.setState({
            getvillage: village.map((i, index) => {
              return { value: i.village, label: i.village };
            })
          });
        } else {
          this.setState({ village: "à¸­à¸·à¹ˆà¸™à¹†", village2: "" });
        }
      });
    } else if (item1 == "village") {
      this.setState({ village: item2 });
    }
  }
  getData() {
    AsyncStorage.getItem("userid").then(userid => {
      let id = parseInt(userid);
      Axios.get("http://npcrapi.netpracharat.com/api/saveuser/detail/" + id)
        .then(result => {
          this.setState({
            save_name: result.data.save_name,
            save_lastname: result.data.save_lastname,
            save_citizen: result.data.save_citizen,
            headlines: result.data.headlines,
            email: result.data.email,
            point: result.data.point,
            type: result.data.type,
            address_number: result.data.address_number,
            province: result.data.province,
            district: result.data.district,
            subdistrict: result.data.subdistrict,
            village: result.data.village,
            village2: result.data.village
          });
          this.getDataProvince();
          // alert(JSON.stringify(result.data));
        })
        .catch(err => {
          this.getData();
        });
    });
  }

  componentDidMount() {
    this.getData();
  }


  render() {
    const placeholder = {
      // gender: {
      //   label: 'กรุณาเลือกเพศ',
      //   // value: null,
      //   color: '#8e8e8e',
      // },
    }


    return (

      <ScrollView showsVerticalScrollIndicator={false} >
        <KeyboardAwareScrollView>
          <View style={styles.container}>
            {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(5.5), fontWeight: '500' }} >เครือข่ายอาสาสมัคร (อสด.)</Text> */}
            {/* <Image source={require('../assets/component/Register.png')} style={{ width: wp(56), height: wp(58.5), marginTop: wp(5), resizeMode: 'contain' }} /> */}
            {/* <Button
          title="Click Here"
          onPress={() => alert('Button Clicked!')}
        /> */}
            <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(5.5), marginTop: wp(3), fontWeight: '400' }} >ลงทะเบียนเพื่อเข้าสู่ระบบ</Text>



            <View style={{ width: '100%', alignItems: 'center' }}>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('2.5%'), fontWeight: '300', }} >ชื่อจริง</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp(0), fontWeight: '500', color: '#F5D540' }} >*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15, }}>

                <TextInput
                  // maxLength={10}
                  placeholder={'กรุณากรอกชื่อจริง'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-2)
                  }}
                  onChangeText={(text) => this.setState({ Name: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >หมายเลขบัตรประชาชน</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1.7%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15 }}>

                <TextInput
                  maxLength={13}
                  placeholder={'กรุณากรอกเลขบัตรประชาชน'}
                  placeholderTextColor='grey'
                  keyboardType='numeric'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-2)
                  }}
                  onChangeText={(text) => this.setState({ Nationalid: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >เพศ</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>


              <View style={styles.dropdown}>
                <RNPickerSelect
                  Icon={() => {
                    return (
                      <Icon
                        underlayColor={false}
                        // raised
                        name="ios-arrow-down"
                        type="ionicon"
                        color="grey"
                        size={wp(7)}
                        // containerStyle={{ marginLeft: wp(-10), marginTop: wp(1) }}
                        style={{ marginRight: wp(3), marginTop: wp(1.5) }}
                      />
                    );
                  }}
                  style={pickerSelectStyles}
                  onValueChange={(value) => {
//  console.log(value)
                     
                    if(value !='male'){
                     this.setState({gender : "male"})
                    }
                    
                    
                    
                    else if (value  != 'female'){
                      this.setState({gender : "female"})
                    }

                        console.log(this.state.gender)

                    
                  }}
                  placeholderColor='#00'
                  placeholder={{
                    label: "กรุณาเลือกเพศ",
                   // value: "",

                  }}
                  items={[
                    { label: 'ชาย', value: 'male' },
                    { label: 'หญิง', value: 'female' },
                  ]}
                  useNativeAndroidPickerStyle={false}
                // value={this.state.gender}
                />
              </View>
              {/* 
              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('-1%'), fontWeight: '300', }} >ปีเกิด</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('-4%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>

              <View style={styles.dropdown}>
                <DatePicker
                  // Icon={() => {
                  //   return (
                  //     <Icon
                  //       underlayColor={false}
                  //       // raised
                  //       name="ios-arrow-down"
                  //       type="ionicon"
                  //       color="grey"
                  //       size={wp(7)}
                  //       // containerStyle={{ marginLeft: wp(-10), marginTop: wp(1) }}
                  //       style={{ marginLeft: wp(-8), marginTop: wp(1.5) }}
                  //     />
                  //   );
                  // }}
                  style={{ width: wp(90) ,marginTop:wp(-1) }}
                  date={this.state.dob}
                  mode="date"
                  androidMode="spinner"
                  placeholder= {<Text style={{ fontFamily:'Prompt-Light',fontSize:wp(3.5), color:'grey'}}>เลือกวันเกิด</Text>}
                 
                  format="YYYY-MM-DD"
                  //minDate="2016-05-01"
                  //maxDate="2016-06-01"
                  confirmBtnText="ยืนยัน"
                  cancelBtnText="ยกเลิก"
                  customStyles={{
                    dateIcon: {
                      marginLeft: 0,
                      marginTop: wp(0)
                    },
                    dateInput: {
                      marginLeft: wp(2.5),
                      marginTop: wp(0),
                      borderRadius: 15,
                      borderWidth: 0.5,
                      borderColor: 'rgb(242,242,242)',
                      backgroundColor:'rgb(242,242,242)',
                     
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(dob) => { this.setState({ dob: dob }) }}
                />

              </View> */}
              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('1%'), fontWeight: '300', }} >อายุ</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp(-2), fontWeight: '500', color: '#F5D540' }} >*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15, }}>

                <TextInput
                  // maxLength={10}
                  placeholder={'กรุณากรอกอายุ'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ Age: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >ที่อยู่</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('0%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('1%'), borderRadius: 15, }}>

                <TextInput
                  // maxLength={10}
                  placeholder={'กรุณากรอกที่อยู่'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ Address: text })}
                />

              </View>

              <View style={styles.dropdown1}>
                <RNPickerSelect

                  Icon={() => {
                    return (
                      <Icon
                        underlayColor={false}
                        // raised
                        name="ios-arrow-down"
                        type="ionicon"
                        color="grey"
                        size={wp(7)}
                        // containerStyle={{ marginLeft: wp(-10), marginTop: wp(1) }}
                        style={{ marginLeft: wp(-8), marginTop: wp(1.5) }}
                      />
                    );
                  }}
                  placeholderTextColor="#0000000"
                  // Icon={true}
                  // disabled={true}
                  // hideDoneBar={true}
                  // onUpArrow={false}
                  // onDownArrow={false}
                  useNativeAndroidPickerStyle={false}
                  placeholder={{
                    label: "กรุณาระบุจังหวัด",
                    value: "",

                  }}
                  items={this.state.getprovince}
                  onValueChange={(itemValue, itemIndex) => {
                    this.getData2("province", itemValue, itemIndex);
                  }}
                  onUpArrow={() => {
                    this.inputRefs.picker.togglePicker();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.company.focus();
                  }}
                  style={pickerSelectStyles}
                  value={this.state.province}
                // ref={(el) => {
                //     this.inputRefs.picker2 = el;
                // }}
                />
              </View>



              <View style={styles.dropdown}>
                <RNPickerSelect
                  Icon={() => {
                    return (
                      <Icon
                        underlayColor={false}
                        // raised
                        name="ios-arrow-down"
                        type="ionicon"
                        color="grey"
                        size={wp(7)}
                        // containerStyle={{ marginLeft: wp(-10), marginTop: wp(1) }}
                        style={{ marginLeft: wp(-8), marginTop: wp(1.5) }}
                      />
                    );
                  }}
                  placeholderTextColor="#00"
                  // Icon={true}
                  // disabled={true}
                  // hideDoneBar={true}
                  // onUpArrow={false}
                  // onDownArrow={false}
                  useNativeAndroidPickerStyle={false}
                  placeholder={{
                    label: "กรุณาเลือกอำเภอ/เขต",
                    value: ""
                  }}
                  items={this.state.getdistrict}
                  onValueChange={(itemValue, itemIndex) => {
                    this.getData2("district", itemValue, itemIndex);
                  }}
                  onUpArrow={() => {
                    this.inputRefs.picker.togglePicker();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.company.focus();
                  }}
                  style={pickerSelectStyles}
                  value={this.state.district}
                // ref={(el) => {
                //     this.inputRefs.picker2 = el;
                // }}
                />
              </View>

              <View style={styles.dropdown}>
                <RNPickerSelect
                  Icon={() => {
                    return (
                      <Icon
                        underlayColor={false}
                        // raised
                        name="ios-arrow-down"
                        type="ionicon"
                        color="grey"
                        size={wp(7)}
                        // containerStyle={{ marginLeft: wp(-10), marginTop: wp(1) }}
                        style={{ marginLeft: wp(-8), marginTop: wp(1.5) }}
                      />
                    );
                  }}
                  placeholderTextColor="#00"
                  // Icon={true}
                  // disabled={true}
                  // hideDoneBar={true}
                  // onUpArrow={false}
                  // onDownArrow={false}
                  useNativeAndroidPickerStyle={false}
                  placeholder={{
                    label: "กรุณาเลิอกตำบล/แขวง",
                    value: ""
                  }}
                  items={this.state.getsubdistrict}
                  onValueChange={(itemValue, itemIndex) => {
                    this.getData2("sub_district", itemValue, itemIndex);
                  }}
                  onUpArrow={() => {
                    this.inputRefs.picker.togglePicker();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.company.focus();
                  }}
                  style={pickerSelectStyles}
                  value={this.state.subdistrict}
                // ref={(el) => {
                //     this.inputRefs.picker2 = el;
                // }}
                />
              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('1%'), fontWeight: '300', }} >เบอร์ติดต่อกลับ</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('-2%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15 }}>

                <TextInput
                  maxLength={10}
                  placeholder={'กรุณากรอกเบอร์ติดต่อกลับ'}
                  placeholderTextColor='grey'
                  keyboardType='numeric'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ Mobileno: text })}
                />

              </View>


              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >อีเมลล์</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text>
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15 }}>

                <TextInput
                  // maxLength={10}
                  placeholder={'กรุณากรอกอีเมลล์'}
                  placeholderTextColor='grey'
                  keyboardType='email-address'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ email: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >วุฒิการศึกษา</Text>
                {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text> */}
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('1%'), borderRadius: 15 }}>

                <TextInput
                  // maxLength={10}
                  placeholder={'กรุณากรอกวุฒิการศึกษา'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ educational: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >อาชีพ</Text>
                {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text> */}
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('1%'), borderRadius: 15 }}>

                <TextInput
                  //  maxLength={10}
                  placeholder={'กรุณากรอกอาชีพ'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(-1)
                  }}
                  onChangeText={(text) => this.setState({ occupation: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >ความถนัด / ความสนใจ</Text>
                {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text> */}
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15 }}>

                <TextInput
                  //   maxLength={10}
                  placeholder={'กรุณากรอกความถนัด / ความสนใจ'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(0.5)
                  }}
                  onChangeText={(text) => this.setState({ interested: text })}
                />

              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'flex-start', }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp('4%'), fontWeight: '300', }} >ความสามารถพิเศษ</Text>
                {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), marginTop: wp('1%'), fontWeight: '500', color: '#F5D540' }}>*</Text> */}
              </View>

              <View
                style={{ alignItems: 'center', marginTop: wp('0%'), borderRadius: 15 }}>

                <TextInput
                  //  maxLength={10}
                  placeholder={'กรุณากรอกความสามารถพิเศษ'}
                  placeholderTextColor='grey'
                  keyboardType='default'
                  //paddingLeft= {20}

                  style={{
                    fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                    height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15, marginTop: wp(0.5)
                  }}
                  onChangeText={(text) => this.setState({ specialSkill: text })}
                />

              </View>

              <TouchableOpacity
                onPress={() => this.addUser()}
              // style={styles.login}

              >
                {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#F5D540', '#F5D540']}>
                  <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#000' }}>ลงทะเบียน</Text>
                </LinearGradient>
              </TouchableOpacity>


            </View>

          </View>
        </KeyboardAwareScrollView>
      </ScrollView>
    )
  }
}







const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: '0%',
    marginBottom: '10%'


  },

  login: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
    height: wp(10),
    width: wp(75),
    marginTop: wp(2),
    marginTop: wp(10),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 30,
    // borderColor: '#fff'
  },

  dropdown: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: wp(-1),
    marginTop: wp(0)

  },
  dropdown1: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: wp(-1),
    marginTop: wp(3)

  }
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: wp('3.5%'),
    width: wp(85),
    paddingTop: wp(2),
    height: wp(11),
    paddingHorizontal: wp(3),
    paddingBottom: wp(2),
    borderWidth: 2,
    borderColor: "rgb(242,242,242)",
    borderRadius: 15,
    backgroundColor: "rgb(242,242,242)",
    // backgroundColor:'red',
    color: "black",
    alignSelf: "center",
    textAlign: 'center',
    fontFamily: 'Prompt-Light',
    marginTop: wp(-1),
    marginBottom: wp(4)


  },
  inputAndroid: {
    fontSize: 16,
    width: wp(89),
    paddingTop: 13,
    height: wp(12),
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 2,
    borderColor: "red",
    borderRadius: 4,
    backgroundColor: "grey",
    color: "black",
    alignSelf: "center"
  }
});