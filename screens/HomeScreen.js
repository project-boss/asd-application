import React, { Component } from 'react';
import { SafeAreaView, AsyncStorage, Animated, Platform, FlatList, Dimensions, View, Text, Button, StyleSheet, TouchableOpacity, Image } from 'react-native';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';


import Axios from 'axios';
// import { SearchBar } from 'react-native-elements';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';

import WeatherComponent from './header';
import SwiperFlatList from 'react-native-swiper-flatlist';
import IconsHomeComponent from './IconsHome'
import ActivityComponent from './activity'
import CommunityComponent from './community'
import axios from 'axios';




const HEADER_MAX_HEIGHT = wp(60);
const HEADER_MIN_HEIGHT = wp(25);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const { height, width } = Dimensions.get('window')



export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.index = 0;
    this.index1 = 0;
    this.index2 = 0;
    this.index3 = 0;
  }



  state = {
    scrollY: new Animated.Value(0),

  };

  static navigationOptions = {  
    title: 'Home',  
    headerStyle: {  
        backgroundColor: '#f4511e',  
    },  
    //headerTintColor: '#0ff',  
    headerTitleStyle: {  
        fontWeight: 'bold',  
    },  
};  


  componentWillMount() {
    // alert(JSON.stringify(this.props))
    this.scrollY = new Animated.Value(0)

    this.startHeaderHeight = 60
    this.endHeaderHeight = 0
    if (Platform.OS == 'android') {
      this.startHeaderHeight = 100 + StatusBar.currentHeight
      this.endHeaderHeight = 70 + StatusBar.currentHeight
    }

    this.animatedHeaderHeight = this.scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [this.startHeaderHeight, this.endHeaderHeight],
      extrapolate: 'clamp'
    })

    this.animatedOpacity = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    })
    this.animatedTagTop = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [0, 0],
      extrapolate: 'clamp'
    })
    this.animatedMarginTop = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [50, 30],
      extrapolate: 'extend'
    })

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'extend',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });

  }

  render() {

    const HEADER_MAX_HEIGHT = wp('40%');
    const HEADER_MIN_HEIGHT = wp('20%');
    const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'extend',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });


    let data = [
      { id: 1, image: 'https://royalcoastreview.com/wp-content/uploads/2019/06/pm.jpg' },
      { id: 2, image: 'https://media.nationthailand.com/images/news/2019/07/24/30373563/800_06edd4bae3da733.jpg?v=1563953146' },

    ];
 
    return (
      
      <View style={styles.linearGradient}>

        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View >


            <Animated.View style={[{ backgroundColor: 'white', height: headerHeight, }]}>


              <View style={styles.bar}>
                {/* <Text style={styles.title}  >เครือข่ายอาสาสมัคร</Text> */}



              </View>
            </Animated.View>

            <Animated.View
              style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}>

              <WeatherComponent route={this.props.navigation} />

            </Animated.View>



          </View>

          <ScrollView
            style={styles.fill}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
            )}
          >


            <View style={{
              alignSelf: "center",
              flex: 1,
              marginTop: wp(0),
              width: wp(90),
              height:wp(40),
              alignItems: "center",
              //  backgroundColor:'green',

            }} >

              <SwiperFlatList
                autoplay
                // autoplayDelay={5}
                index={0}
                autoplayLoop
                // autoplayInvertDirection
                data={data}
                showPagination
                paginationStyleItem={{ width: wp(2), height: wp(2), borderRadius: 5, marginTop: wp(20), }}
                paginationActiveColor="#F5D540"
                style={{ width: wp(90), height:wp(40), marginTop: wp(0),}}
                renderItem={({ item }) =>

                  <Image style={styles.image} source={{ url: item.image }} />


                }
              
              // initialNumToRender={this.index + 1}
              />



          

            </View>

            <View>
              <IconsHomeComponent route={this.props.navigation} />
            </View>

            <View style={styles.lineStylew1} /> 

            <View>
              <ActivityComponent  route={this.props.navigation} />
            </View>
            <View style={styles.lineStylew2} /> 
            <View>
              <CommunityComponent  route={this.props.navigation} />
            </View>



          </ScrollView>




        </View>
      </View >
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20%'

  },

  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },

  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 15,
    height: wp(50),
    width: wp('100'),

  },

  image: {
    height: wp(40),
    width: wp(90),
    alignItems: 'center',
    alignSelf: 'center',
  //  resizeMode: 'contain'

  },lineStylew1: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(90),
    height: wp(0.5),
    marginTop: wp(6),
    backgroundColor: 'rgb(239, 239, 239)'
},
  lineStylew2: {
  borderWidth: 0.25,
  borderColor: '#DCDCDC',
  alignSelf: 'center',
  width: wp(90),
  height: wp(0.5),
  marginTop: wp(1),
  backgroundColor: 'rgb(239, 239, 239)'
},


  fill: {
    flex: 1,
  },
  bar: {
    marginTop: wp(8),
    height: wp(8),
    alignItems: 'center',
    justifyContent: 'center',

  },
  title: {
    backgroundColor: 'transparent',
    color: 'black',
    fontSize: wp('4.5%'), fontWeight: '500',
    fontFamily: 'prompt-light'

  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },
  bar1: {
    marginTop: wp(20),
    height: wp('8%'),
    alignItems: 'center',
    justifyContent: 'center',

  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },



})





