import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';


export default class IconsHome extends Component {

    constructor(props) {
        super(props);
        this.index = 0;
        this.index2 = 0;
    
      }
    render() {
        
        return (
            
            <View style={styles.container}>
                <View style={{
                    width: wp(90), height: wp(15), backgroundColor: 'rgb(247,247,247)',
                    borderRadius: 8, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'column',
                    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop:wp(5)
                }}>

                </View>

                <View style={{ marginTop: wp(8), alignItem: 'flex-start', justifyContent: 'space-between', width: wp(87), flexDirection: 'row' }} >

                    <View >
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <Image source={require('../assets/component/index/iconKnowledge.png')}
                                style={{ width: wp(20), height: wp(20), }} />
                            <Text style={styles.textIcon}>องค์ความรู้</Text>
                        </TouchableOpacity>
                    </View>

                    <View >
                        <TouchableOpacity style={{ alignItems: 'center' }}  onPress={() => this.props.route.navigate('GeneralNews')} >
                            <Image source={require('../assets/component/index/iconNews.png')}
                                style={{ width: wp(20), height: wp(20), }} />
                            <Text style={styles.textIcon}>ข่าวสารทั่วไป</Text>
                        </TouchableOpacity>
                    </View>
                    <View >
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <Image source={require('../assets/component/index/iconValunteer.png')}
                                style={{ width: wp(20), height: wp(20), }} />
                            <Text style={styles.textIcon}>ข่าวสารอาสา</Text>
                        </TouchableOpacity>
                    </View>


                </View>

                <View style={{
                    width: wp(90), height: wp(22), backgroundColor: "#F5D540",
                    borderRadius: 8, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'row',
                    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop: wp(5), justifyContent: 'space-between',
                }}>

                    <View >
                        <TouchableOpacity  style={{ alignItems: 'center', marginLeft: wp(5) }}>
                        <Image source={require('../assets/component/index/iconManual.png')}
                            style={{ width: wp(10), height: wp(10), resizeMode: 'contain' }} />
                        <Text style={styles.textIcon}>คู่มือการใช้งานแอปพลิเคชั่น</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.lineStyle} ></View>

                    <View >
                        <TouchableOpacity style={{ alignItems: 'center', marginRight: wp(5) }}>
                        <Image source={require('../assets/component/index/icon_Project.png')}
                            style={{ width: wp(9), height: wp(10), resizeMode: 'contain' }} />
                        <Text style={styles.textIcon}>แบบขอยื่นงบประมาณโครงการ</Text>
                        </TouchableOpacity>
                    </View>

                </View>



            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '3%'

    },
    textIcon: {
        fontFamily: 'Prompt-Light',
        fontSize: wp(3),
        fontWeight: '300',
        marginTop:wp(1)
    },
    lineStyle:{
        width:wp(0.5),
        height:wp(17),
        borderWidth:1,
        borderColor: 'rgb(244,234,81)',

    }

});  