import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';
import RegisterScreen from './RegisterScreen';

import SettingsScreen from './SettingsScreen';
import LoginScreen from './LoginScreen'
import ProfileScreen from './ProfileScreen';
import GeneralNews from './GeneralNews'


const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Home"
    activeColor="black"
  >
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: <Text style={{ color: "black", fontFamily:'Prompt-Light' }}>หน้าหลัก</Text>,
        
        tabBarColor: '#F5D540',
        tabBarIcon: () => (
          <Icon name="ios-home" color='black' size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Activity"
      component={DetailsStackScreen}
      options={{
        tabBarLabel: <Text style={{ color: "black", fontFamily:'Prompt-Light' }}>กิจกรรม</Text>,
        tabBarColor: '#F5D540',
        tabBarIcon: () => (
          <Icon name="ios-clipboard" color='black' size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="News"
      component={GeneralNews}
      options={{
        tabBarLabel: <Text style={{ color: "black", fontFamily:'Prompt-Light' }}>ข่าวสารทั่วไป</Text>,
        tabBarColor: '#F5D540',
        tabBarIcon: () => (
          <Icon name="ios-paper" color='black' size={26} />
        ),
      }}
    />

     <Tab.Screen
      name="Login"
      component={LoginScreen}
      options={{
        tabBarLabel: <Text style={{ color: "black", fontFamily:'Prompt-Light' }}>โปรไฟล์</Text>,
        tabBarColor: '#F5D540',
        tabBarIcon: () => (
          <Icon name="md-person" color='black' size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Notification"
      component={ProfileScreen}
      options={{
        tabBarLabel: <Text style={{ color: "black", fontFamily:'Prompt-Light' }}>แจ้งเตือน</Text>,
        tabBarColor: '#F5D540',
        tabBarIcon: () => (
         
          <Icon name="md-notifications" color='black' size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const HomeStackScreen = ({ navigation }) => (

  <HomeStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#F5D540',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',

    }
  }}>

    <HomeStack.Screen name="Home" component={HomeScreen}
      options={{
        headerShown: false,
        // headerLeft: () => (
        //     <Icon.Button name="ios-menu" size={25}  onPress={() => navigation.openDrawer()}></Icon.Button>
        // )
      }} />
  </HomeStack.Navigator>
);

const DetailsStackScreen = ({ navigation }) => (
  <DetailsStack.Navigator >

    <DetailsStack.Screen name="กิจกรรมอาสา" component={DetailsScreen} options={{
      headerShown: false,
      headerLeft: () => (
        <Icon name="ios-arrow-back" size={25} onPress={() => navigation.goBack()} />
      )
    }} />
  </DetailsStack.Navigator>
);


// const WebNews = ({ navigation }) => (
//   <WebNews.Navigator >

//     <WebNews.Screen name="กิจกรรมอาสา" component={WebNews} options={{
//       // headerShown: false,
//       headerLeft: () => (
//         <Icon name="ios-arrow-back" size={25} onPress={() => navigation.goBack()} />
//       )
//     }} />
//   </WebNews.Navigator>
// );



// const allweatherscreen = ({ navigation }) => (
//   <allweatherscreen.Navigator>

//     <allweatherscreen.Screen  name="กิจกรรมอาสา" component={allweatherscreen} options={{
//       headerShown: false,
//       // headerLeft: () => (
//       //   <Icon name="ios-arrow-back" size={25} onPress={() => navigation.goBack()} />
//       // )
//     }} />
//   </allweatherscreen.Navigator>
// );
