

import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, ScrollView, Image, TouchableOpacity, TextInput, FlatList } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import RegisterScreen from './RegisterScreen'

export default class LoginScreen extends Component {


  constructor(props) {
    super(props);
    this.state = {
      type: '1',
      phoneNumber: ''

    };


  }

  // renderItem(item) {
  //   return (
  //     <View style={{ width: wp(70), height: wp(70), resizeMode: 'contain', }} >
  //       {/* <Image source={require('../assets/component/Login.png')} style={{ width: wp(70), height: wp(70), resizeMode: 'contain', }} /> */}

  //     </View>
  //   )
  // }

  onViewableItemsChanged = ({ viewableItems, changed, item }) => {
    this.setState({
      type: viewableItems[0].item.id,

    });
    //console.log(JSON.stringify(viewableItems));
    console.log(JSON.stringify(this.state.type));

    // console.log("Changed in this iteration", changed);
  };
  viewabilityConfig = { viewAreaCoveragePercentThreshold: 50 };

  render() {

    let data = [
      { id: 1, pic: require('../assets/component/Login.png') },
      { id: 2, pic: require('../assets/component/Register.png') },

    ];


    return (
      <ScrollView  showsVerticalScrollIndicator={false} >

        <View style={styles.container}>
          <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(5.5), fontWeight: '500' }} >เครือข่ายอาสาสมัคร (อสด.)</Text>


          <View style={{
            alignSelf: "center",
            flex: 1,
            marginTop: wp(15),
            width: wp(90),
            alignItems: "center"

          }} >


            <FlatList
              data={data}
              horizontal
              onViewableItemsChanged={this.onViewableItemsChanged}
              viewabilityConfig={this.viewabilityConfig}
              pagingEnabled

              style={{ width: wp(70), }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item) => item.id}
              // ref={ref => (this.flatlist = ref)}
              renderItem={({ item, index }) => {

                return (
                  <View style={{ flexDirection: 'row' }} >
                    <Image source={item.pic} style={{
                      resizeMode: 'contain',
                      borderRadius: 10,
                      height: wp(70),
                      width: wp(70)
                    }} />
                  </View>
                )
              }} />

            {/* <Icon style={{}} name="ios-arrow-forward" size={30} color="grey" onPress={() => { }} /> */}


          </View>

          <View style={{ flexDirection: "row", alignSelf: "center", marginTop: wp(2), }}>

            <View
              style={{
                borderRadius: wp(3) / 2,
                height: wp(3),
                width: wp(3),
                backgroundColor:
                  this.state.type == "1"
                    ? "#F5D540"
                    : "rgba(80,80,80,0.5)",
                margin: wp(1)
              }}
            />
            <View
              style={{
                borderRadius: wp(3) / 2,
                height: wp(3),
                width: wp(3),
                backgroundColor:
                  this.state.type != "1"
                    ? "#F5D540"
                    : "rgba(80,80,80,0.5)",
                margin: wp(1)
              }}
            />
          </View>
          {this.state.type == '1' ?
            <View style={{ alignItems: 'center' }}>

              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(5.5), marginTop: wp(1), fontWeight: '400' }} >กรุณาลงชื่อเข้าสู่ระบบ</Text>

              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp(4), fontWeight: '300' }} >กรุณากรอกหมายเลขโทรศัพท์</Text>
              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), marginTop: wp(1), fontWeight: '300' }} >ที่ได้ทำการลงทะเบียนไว้ในระบบเรียบร้อยแล้ว</Text>


              <View
                style={{ alignItems: 'center', marginTop: wp('7%'), height: wp('12%'), width: wp('85%'), borderRadius: 15, }}>
                <KeyboardAwareScrollView>
                  <TextInput
                    maxLength={10}
                    placeholder={'กรุณากรอกหมายเลขโทรศัพท์'}
                    placeholderTextColor='grey'
                    keyboardType='numeric'
                    //paddingLeft= {20}

                    style={{
                      fontSize: wp('3.5%'), width: wp('85%'), textAlign: 'center', fontFamily: 'Prompt-Light',
                      height: wp('11%'), backgroundColor: 'rgb(242,242,242)', borderRadius: 15
                    }}
                    onChangeText={text => this.state.phoneNumber}
                  />
                </KeyboardAwareScrollView>

              </View>



              <TouchableOpacity
                onPress={() => { }}
              // style={styles.login}

              >
                {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#F5D540', '#F5D540']}>
                  <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#000', }}>เข้าสู่ระบบ</Text>
                </LinearGradient>
              </TouchableOpacity>


            </View> :
            <View>

              <RegisterScreen route={this.props.navigation} />
            </View>

          }


        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {

    alignItems: 'center',
    justifyContent: 'flex-start',

    marginTop: '20%',
    marginBottom: wp(10),
  },

  login: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
    height: wp(10),
    width: wp(75),

    marginTop: wp(10),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 30,
    // borderColor: '#fff'
  },
});