

// import React, { Component } from 'react';
// import { View, Text, Image, StyleSheet, ImageBackground, Alert } from 'react-native';
// import LinearGradient from 'react-native-linear-gradient';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
//   listenOrientationChange as loc,
//   removeOrientationListener as rol
// } from 'react-native-responsive-screen';
// import axios from 'axios';
// import Moment from 'moment';
// import { TouchableHighlight } from 'react-native-gesture-handler';
// navigator.geolocation = require('@react-native-community/geolocation');

// class WeatherComponent extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       latitude: 0,
//       longitude: 0,
//       data: []
//     };
//   }
//   getLocation() {

//     // Get the current position of the user

//     navigator.geolocation.getCurrentPosition(
//       (position) => {
//         this.setState(
//           (prevState) => ({
//             latitude: position.coords.latitude,
//             longitude: position.coords.longitude
//           }), () => { this.getData(); }
//         );
//       },
//       (error) => this.setState({ forecast: error.message }),
//       { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
//     );
//   }


//   getData() {
//     // alert(this.state.latitude+','+this.state.longitude)
//     //  axios.get('http://npcrapi.netpracharat.com:3000/latlng/13.8849447,100.57561149999992')
//     axios.get('http://npcrapi.netpracharat.com:3000/latlng/' + this.state.latitude + ',' + this.state.longitude)
//       .then(result => {
//         let data = result.data.data[0].SevenDaysForecast
//         var today = data.slice(0, 1);
//         this.setState({
//           data: today,
//           province: result.data.data[0].ProvinceNameTh
//         })
//         console.log(JSON.stringify(data))
//       })
//   }
//   componentDidMount() {
//     this.getLocation()
//   }
//   render() {

//     var today = Moment();
//     let thaiday = Moment(today).format('ddd');
//     let day = Moment(today).format('D');
//     let thaiday1 = thaiday == 'Mon' ? 'วันจันทร์' :
//       thaiday == 'Tue' ? 'วันอังคาร' :
//         thaiday == 'Wed' ? 'วันพุธ' :
//           thaiday == 'Thu' ? 'วันพฤหัสบดี' :
//             thaiday == 'Fri' ? 'วันศุกร์' :
//               thaiday == 'Sat' ? 'วันเสาร์' : 'วันอาทิตย์'

//     let thaimount = Moment(today).format('M')
//     let thaimount1 = thaimount == 1 ? 'ม.ค.' :
//       thaimount == 2 ? 'ก.พ.' :
//         thaimount == 3 ? 'มี.ค.' :
//           thaimount == 4 ? 'เม.ย.' :
//             thaimount == 5 ? 'พ.ค.' :
//               thaimount == 6 ? 'มิ.ย.' :
//                 thaimount == 7 ? 'ก.ค.' :
//                   thaimount == 8 ? 'ส.ค.' :
//                     thaimount == 9 ? 'ก.ย.' :
//                       thaimount == 10 ? 'ต.ค.' :
//                         thaimount == 11 ? 'พ.ย.' : 'ธ.ค.'
//     // var today = Momentเ();
//     const tomorrow = Moment().add(1, 'days');
//     // var time = 6
//     var time = Moment().format('H');
//     var color_font = time < 18 && time > 5 ? "#000" : '#FFF'

//     return (
//       this.state.data[0] != null ?
//         // <TouchableHighlight onPress={() => alert('in')}>
//         <TouchableHighlight onPress={() => this.props.route.navigate('allweather')}>


//           <ImageBackground source={time < 18 && time > 5 ? require('../assets/component/HomeNewTOTeasylifeAW124.png') :
//             require('../assets/component/HomeNewTOTeasylifeAW123.png')} imageStyle={{ resizeMode: 'stretch', }} style={styles.linearGradient}>
//             {/* Today  */}
//             <View style={{ flex: 1, }}>
//               <View style={{ alignItems: 'center', flexDirection: 'row' }}>
//                 <View style={{flexDirection:'column'}} >
//                   <Text
//                     style={{
//                       fontFamily: 'Prompt-Light',
//                       fontSize: wp(4.5),
//                       fontWeight: '400',
//                       letterSpacing: 3.0,
//                       color: color_font,
//                       marginTop: wp('12%')
//                     }}>
//                     {this.state.province}

//                   </Text>


//                   <Text
//                     style={{
//                       fontFamily: 'Prompt-Light',
//                       fontSize: wp(3.5),
//                       fontWeight: '500',
//                       color: color_font,


//                     }}>
//                     {this.state.data[0].WeatherDescription}
//                   </Text>
//                 </View>

//                 <View style={{ flexDirection: 'row', backgroundColor:'red', alignItems:'center', justifyContent:'center' }}>

//                   <Text
//                     style={{
//                       fontFamily: 'Prompt-Light',
//                       fontSize: wp(12),
//                       marginTop:wp(5),
//                       letterSpacing: 2.0,
//                       fontWeight: '400',
//                       color: color_font

//                     }}>

//                     {/* {this.state.data[0].MinTemperature.Value + '-' + this.state.data[0].MaxTemperature.Value + '\u00B0'} */}
//                     {this.state.data[0].MaxTemperature.Value}
//                   </Text>

//                   <Text
//                     style={{
//                       fontFamily: 'Prompt-Light',
//                       fontSize: wp(10),

//                       letterSpacing: 2.0,
//                       fontWeight: '500',
//                       color: color_font
//                     }}>

//                     {/* {this.state.data[0].MinTemperature.Value + '-' + this.state.data[0].MaxTemperature.Value + '\u00B0'} */}
//                     {'\u00B0'}
//                   </Text>
//                 </View>
//               </View>



//             </View>

//           </ImageBackground>
//         </TouchableHighlight> : null

//     );
//   }
// }
// const styles = StyleSheet.create({
//   card: {
//     backgroundColor: 'rgba(56, 172, 236, 1)',
//     borderWidth: 0,
//     borderRadius: 20
//   },
//   linearGradient: {
//     height: wp('50%'),
//     width: wp('100%'),

//     // borderRadius: 5
//   },


// });
// export default WeatherComponent;

import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import axios from 'axios';
import Moment from 'moment';
import { TouchableHighlight } from 'react-native-gesture-handler';
navigator.geolocation = require('@react-native-community/geolocation');


class WeatherComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      data: []
    };
  }
  getLocation() {

    // Get the current position of the user
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(
          (prevState) => ({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }), () => { this.getData(); }
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }


  getData() {
    // alert(this.state.latitude+','+this.state.longitude)
    // axios.get('http://npcrapi.netpracharat.com:3000/latlng/13.8849447,100.57561149999992')
    axios.get('http://npcrapi.netpracharat.com:3000/latlng/' + this.state.latitude + ',' + this.state.longitude)
      .then(result => {
        let data = result.data.data[0].SevenDaysForecast
        var today = data.slice(0, 1);
        this.setState({
          data: today,
          province: result.data.data[0].ProvinceNameTh
        })
        // alert(JSON.stringify(today))
      })
  }
  componentDidMount() {
    this.getLocation()
  }
  render() {

    var today = Moment();
    let thaiday = Moment(today).format('ddd');
    let day = Moment(today).format('D');
    let thaiday1 = thaiday == 'Mon' ? 'วันจันทร์' :
      thaiday == 'Tue' ? 'วันอังคาร' :
        thaiday == 'Wed' ? 'วันพุธ' :
          thaiday == 'Thu' ? 'วันพฤหัสบดี' :
            thaiday == 'Fri' ? 'วันศุกร์' :
              thaiday == 'Sat' ? 'วันเสาร์' : 'วันอาทิตย์'

    let thaimount = Moment(today).format('M')
    let thaimount1 = thaimount == 1 ? 'ม.ค.' :
      thaimount == 2 ? 'ก.พ.' :
        thaimount == 3 ? 'มี.ค.' :
          thaimount == 4 ? 'เม.ย.' :
            thaimount == 5 ? 'พ.ค.' :
              thaimount == 6 ? 'มิ.ย.' :
                thaimount == 7 ? 'ก.ค.' :
                  thaimount == 8 ? 'ส.ค.' :
                    thaimount == 9 ? 'ก.ย.' :
                      thaimount == 10 ? 'ต.ค.' :
                        thaimount == 11 ? 'พ.ย.' : 'ธ.ค.'
    // var today = Momentเ();
    const tomorrow = Moment().add(1, 'days');
    // var time = 6
    var time = Moment().format('H');
    var color_font = time < 18 && time > 5 ? "#000" : '#FFF'

    return (
      this.state.data[0] != null ?
        <TouchableHighlight onPress={() => this.props.route.navigate('allweather')}>

          <ImageBackground source={time < 18 && time > 5 ? require('../assets/component/index/Head_BG.png') :
            require('../assets/component/index/Head_BG.png')} imageStyle={{ resizeMode: 'stretch', height:wp(70)}} style={styles.linearGradient}>
            <View style={{ flexDirection: 'row', width: '100%', }}>
              {/* Today  */}
              <View style={{ flexDirection: 'column', flex: 1, marginTop: wp('10%') }}>
                <View style={{ width: wp('50%'), alignItems: 'flex-start', marginLeft: wp('5%'), marginTop: wp('2%') }}>

                  <Text
                    style={{
                      fontFamily: 'Prompt-Light',
                      fontSize: wp(5.5),
                      fontWeight: '400',
                      letterSpacing: 1.0,
                      color: color_font
                    }}>
                    {this.state.province}

                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={{
                        fontFamily: 'Prompt-Light',
                        fontWeight: '300',
                        fontSize: wp(3.9),
                        color: color_font,


                      }}>
                      {thaiday1}
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Prompt-Light',
                        fontWeight: '300',
                        fontSize: wp(3.5),
                        marginLeft: wp(1),
                        color: color_font
                      }}>
                      {day + ' ' + thaimount1}
                    </Text>

                  </View>
                  <Text
                    style={{
                      fontFamily: 'Prompt-Light',
                      fontSize: wp(3.5),
                      fontWeight: '300',
                      color: color_font,
                      letterSpacing: 1.0,
                    }}>
                    {this.state.data[0].WeatherDescription}
                  </Text>


                </View>

                <View style={{ alignItems: 'flex-end', flex: 1, marginTop: wp('1.2%'), }}>



                </View>

              </View>

              <View style={{ flexDirection: 'row', marginTop:wp(10)}}>
                <Image source={{ uri: 'http://203.113.11.192:3000/weather/' + this.state.data[0].WeatherDescription + '.png' }}
                  style={{
                    width: wp('20%'),
                    height: wp('20%'),
                    marginTop: wp(1),

                  }} />

                <Text
                  style={{
                    fontFamily: 'Prompt-Light',
                    fontSize: wp(12),
                    marginTop: wp(2),
                    letterSpacing: 2.0,
                    fontWeight: '300',
                    color: color_font

                  }}>

                  {/* {this.state.data[0].MinTemperature.Value + '-' + this.state.data[0].MaxTemperature.Value + '\u00B0'} */}
                  {this.state.data[0].MaxTemperature.Value}
                </Text>

                <Text
                  style={{
                    fontFamily: 'Prompt-Light',
                    fontSize: wp(10),
                    marginTop: wp(1),
                    letterSpacing: 2.0,
                    fontWeight: '300',
                    color: color_font
                  }}>

                  {/* {this.state.data[0].MinTemperature.Value + '-' + this.state.data[0].MaxTemperature.Value + '\u00B0'} */}
                  {'\u00B0'}
                </Text>

              </View>
            </View>



          </ImageBackground>
        </TouchableHighlight> : null

    );
  }
}
const styles = StyleSheet.create({
  card: {
    backgroundColor: 'rgba(56, 172, 236, 1)',
    borderWidth: 0,
    borderRadius: 20
  },
  linearGradient: {
    height: wp('50%'),
    width: wp('100%'),
    paddingLeft: wp('2%'),
    paddingRight: wp('10%'),

    // borderRadius: 5
  },


});
export default WeatherComponent;