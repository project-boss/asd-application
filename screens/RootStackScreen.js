import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';
import MainTabScreen from './MainTabScreen';
import allweatherscreen from './allweatherscreen';
import RegisterScreen from './RegisterScreen';
import LoginScreen from './LoginScreen';
import GeneralNews from './GeneralNews';
import WebNews from './WebNews'



const RootStack = createStackNavigator();

const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator headerMode='none'mode="modal" 
    >
        {/* <RootStack.Screen name="SplashScreen" component={SplashScreen} /> */}
        {/* <RootStack.Screen name="SignInScreen" component={SignInScreen} /> */}
        {/* <RootStack.Screen name="SignUpScreen" component={SignUpScreen} /> */}
        {/* <RootStack.Screen name="Home" component={HomeScreen} /> */}
        
        <RootStack.Screen name="MainTab" component={MainTabScreen} />
        <RootStack.Screen name="Details" component={DetailsScreen} />
        <RootStack.Screen name="allweather"  component={allweatherscreen}/>
        <RootStack.Screen name="Register"  component={RegisterScreen}/>
        <RootStack.Screen name="GeneralNews"  component={GeneralNews}/>
        <RootStack.Screen name="Login"  component={LoginScreen}/>
        <RootStack.Screen name="WebNews"  component={WebNews}
       
        
        />

    </RootStack.Navigator>
    
);

export default RootStackScreen;