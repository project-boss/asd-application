import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from './HomeScreen';
import ImageSelecter from 'react-native-image-picker';
import { ScrollView } from 'react-native-gesture-handler';

export default class ProfileScreen extends Component {

  SelectCameraRoll = () => {
    //   ImagePicker.openPicker({
    //     width: 300,
    //     height: 300,
    //     cropping: true,
    //     includeBase64: true,
    //     includeExif: true,
    // }).then(image => {
    //     console.log('received base64 image');
    //     this.setState({
    //         imageSource: {uri: 'data:image/jpeg;base64,'+image.data}
    //     });
    // })
    const options = {
      // mediaType:'photo',
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImageSelecter.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        // alert('response image ....')
        // const source = { uri: data:${response.data.mime};base64, + response.data };
        // let source = { uri: response.uri };
        // console.log(response);
        // alert(JSON.stringify(response.data))
        this.state.loading = true


        axios.post(
          "http://203.113.11.167/api/saveimage",
          {
            id: this.state.id,
            userImage: response.data
          }
        ).then(result => {

          Alert.alert(
            "เปลี่ยนรูปโปร์ไฟล์สำเร็จ",
            "",

            [
              { text: 'ตกลง', onPress: () => this.success1() },
            ],
            { cancelable: false },
          );
          this.state.loading = true
          this.setState({
            UserImage: result.data.userImage
          })

          // alert(JSON.stringify(this.state.UserImage));

          // this.setState({
          //     UserImage: 'data:image/jpeg;base64,' + response.data,
          // });
          // Alert.alert("เสร็จสิ้น");

        });

        // this.setState({
        //     UserImage: {
        //         uri: 'data:image/jpeg;base64,' + response.data,
        //     }
        // });
        // alert();
        //this.uploadPhoto(this.state.citizen);
      }
    });
  };


  render() {
    const { goBack } = this.props.navigation;
    return (
      <View>
        <View style={styles.imageComponent}>
          <TouchableOpacity style={{ marginLeft: wp(5), marginTop: wp(-22) }} >
            <Icon name='ios-arrow-back' size={30} onPress={() => this.props.navigation.navigate('Home')} />

          </TouchableOpacity>


          <View style={{ marginTop: wp('5%'), alignSelf: 'center', backgroundColor: 'transperent', width: wp('80'), alignItems: 'center' }} >
            <View style={{ width: wp('30%'), height: wp('30%'), borderRadius: wp('30%') / 2, borderColor: 'rgb(244,230,160)', borderWidth: wp(0.7), }}>
              {/* {

                                    this.state.UserImage == null ? */}
              <Image
                source={require('../assets/component/Logoอสด.png')}
                style={{
                  width: wp('27%'), height: wp('27%'), borderRadius: wp('27%') / 2, alignSelf: 'center', marginTop: wp('0.5%')

                }}
              />
              {/* :

                                        <Image
                                            // source={{ uri: 'http://203.113.11.167/uploads/imagesaveuser/' + this.state.UserImage }}
                                            source={require('../assets/component/Logoอสด.png')}
                                            style={{
                                                width: wp('41%'), height: wp('41%'), borderRadius: wp('41%') / 2, alignSelf: 'center', marginTop: wp('1.3%')

                                            }}
                                        />


                                } */}
            </View>

            <View style={{ marginTop: wp('5%'), flexDirection: 'row', alignSelf: 'center', }} >
              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('4%'), color: '#000', fontWeight: '400' }}>ชื่อ</Text>
              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('4%'), color: '#000', fontWeight: '400' }}>เครือข่ายอาสาสามัคร (อสด.)</Text>

            </View>
            <TouchableOpacity
              style={{ width: wp('11%'), height: wp('11%'), borderRadius: wp('11%') / wp('2%'), marginTop: wp('-19%'), marginLeft: wp('17%'), }}
              onPress={() => this.SelectCameraRoll()}>
              <Image
                source={require('../assets/component/IconCamera.png')}
                style={{ width: wp(10), height: wp(10), borderRadius: wp(10) / 2, }}
              />
            </TouchableOpacity>




          </View>


        </View>

        <ScrollView 
        showsVerticalScrollIndicator={false}
        >
          <View style={{ width: wp(85), marginLeft: wp(5), }} >
            <Text style={styles.text}>หมายเลขบัตรประชาชน</Text>
            <Text style={styles.textform}>1102332345889</Text>

            <Text style={styles.text}>วันเกิด</Text>
            <Text style={styles.textform}>09 กันยายน 2529</Text>

            <Text style={styles.text}>วันเกิด</Text>
            <Text style={styles.textform}>09 กันยายน 2529</Text>

            <Text style={styles.text}>เพศ</Text>
            <Text style={styles.textform}>ชาย</Text>


          </View>

          <View style={styles.lineStylew1} />

          <View style={{ width: wp(85), marginLeft: wp(5), }} >
            <Text style={styles.text}>อีเมลล์</Text>
            <Text style={styles.textform}>testmember@gmail.com</Text>

            <Text style={styles.text}>เบอร์โทรศัพท์</Text>
            <Text style={styles.textform}>0870733380</Text>

            <Text style={styles.text}>ที่อยู่</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text1}>จังหวัด </Text>
              <Text style={styles.textform}>กรุงเทพมหานคร</Text>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text1}>ตำบล/แขวง </Text>
              <Text style={styles.textform}>บางมด</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text1}>อำเภอ/เขต </Text>
              <Text style={styles.textform}>ทุ่งครุ</Text>
            </View>

            <View style={styles.lineStylew1} />
          </View>

          <View style={{ width: wp(85), marginLeft: wp(5), }} >
            <Text style={styles.text}>อาชีพ</Text>
            <Text style={styles.textform}>พนักงานรัฐวิสาหกิจ</Text>

            <Text style={styles.text}>ความถนัด / ความสนใจ</Text>
            <Text style={styles.textform}>โปรแกรมคอมพิวเตอร์ MS Office</Text>

            <Text style={styles.text}>ความสามารถพิเศษ</Text>
            <Text style={styles.textform}>ห่อลิ้น</Text>
          
     
          </View>
          <View style={styles.lineStylew2} />
         

        </ScrollView>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  imageComponent: {
    //   flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    //   marginTop: wp(13)
    backgroundColor: '#F5D540',
    height: wp(65),
    flexDirection: 'row',

  },
  text: {
    fontFamily: 'Prompt-Light',
    fontSize: wp(4),
    fontWeight: '300',
    color: 'grey',
    marginTop: wp(5)
  },
  text1: {
    fontFamily: 'Prompt-Light',
    fontSize: wp(4),
    fontWeight: '300',
    color: 'grey',
    marginTop: wp(0)
  },
  textform: {
    fontFamily: 'Prompt-Light',
    fontSize: wp(4),
    fontWeight: '300',
    marginTop: wp(0)
  },
  lineStylew1: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(90),
    height: wp(0.5),
    marginTop: wp(6),
    backgroundColor: 'rgb(239, 239, 239)'
    
  },
  lineStylew2: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(90),
    height: wp(0.5),
    marginTop: wp(6),
    backgroundColor: 'rgb(239, 239, 239)',
    marginBottom:'80%'
    
  },
});
