
import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, Image,  TouchableOpacity, Modal,Linking } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
// import Faded from '../components/Fade/Faded';
// import { Icon } from 'react-native-elements'
// import Share from 'react-native-share'
import Modal2 from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
import { Icon } from 'react-native-elements';


const HEADER_MAX_HEIGHT = wp(66);
const HEADER_MIN_HEIGHT = wp(20);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


const HEIGHT = wp(55);


const items = [
    { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000', },
    { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },
    { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },
    { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },

];


// badge: 3
export default class GeneralNews extends Component {

    constructor(props) {
        super(props);
        this.index = 0;
        this.index2 = 0;

    }

    state = {
        test: null,
        zoomImage: false,
        NewFirst: [],
        ListNew: [],
        dataSource: {},
        scrollY: new Animated.Value(0),
        pickerDisplayed: false,

        data_like: {
            status: true
        },
        data_share: {
            status: true
        },


        ListNews: [],



    }


    static navigationOptions = ({ navigation }) => {
        // const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)
        return {
            headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>ข่าวสารประชาสัมพันธ์</Text>),

            headerRight: (
                <TouchableOpacity style={{ marginRight: wp(2.5) }} >
                    {/* <Ionicons name='ios-menu' size={30} color='#000' /> */}
                </TouchableOpacity>
            ),
            // //  headerTitle: <Logo/>,

            headerLeft: (
                <TouchableOpacity style={{ marginLeft: wp(3), }} >
                    <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
                </TouchableOpacity>
            ),
            headerStyle: {
                backgroundColor: '#FFF'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,

            },
        }
    };

    isLegitIndex(index, length) {
        if (index < 0 || index >= length) return false;
        return true;
    }


    pagination = (velocity) => {
        let nextIndex;
        if (Platform.OS == "ios")
            nextIndex = velocity > 0 ? this.index + 1 : this.index - 1;
        else
            nextIndex = velocity < 0 ? this.index + 1 : this.index - 1;
        if (this.isLegitIndex(nextIndex, this.state.ListNews.length)) {
            this.index = nextIndex;
        }
        this.flatlist.scrollToIndex({ index: this.index, animated: true });
    }

    share() {
        let shareOptions = {

            title: 'เครือข่ายเน็ตอาสาประชารัฐ',
            message: 'Test',
            url: 'https://npcr.netpracharat.com/News/NewsCm/Detail.aspx?id=6980',
            subject: "Share Link" //  for email
        };
        Share.open(shareOptions)
        // this.point_1('share')
    }

    isLegitIndex1(index, length) {
        if (index < 0 || index >= length) return false;
        return true;
    }

    pagination2 = (velocity) => {
        let nextIndex;
        if (Platform.OS == "ios")
            nextIndex = velocity > 0 ? this.index2 + 1 : this.index2 - 1;
        else
            nextIndex = velocity < 0 ? this.index2 + 1 : this.index2 - 1;
        if (this.isLegitIndex1(nextIndex, this.state.ListNews.length)) {
            this.index2 = nextIndex;
        }
        this.flatlist2.scrollToIndex({ index: this.index2, animated: true });
    }

    getNew() {
        const url = "http://npcrapi.netpracharat.com:3000/kao";

        console.log(url);
        axios.get(url)
            .then(result => {
                console.log(result)
                // alert(JSON.stringify(result.data))
                var data = result.data.data
                console.log(data)
                this.setState({
                    //   NewFirst: result.data[0],
                    ListNews: data
                })
                //    alert(JSON.stringify(this.ListNews))
                console.log(this.state.ListNews, 'in');
            })

    }

    componentDidMount() {
        this.getNew();
        var that = this;
        let items = Array.apply(null, Array(4)).map((v, i) => {
            return { id: i, src: 'http://placehold.it/200x200?text=' + (i + 1) };
        });
        that.setState({
            dataSource: items,
        });
    }


    // renderItem(item) {
        
    //     // const { } = style;
    //     return (
    //         <View style={styles.container}>
    //             <View style={{
    //                 width: wp(95), marginTop: wp(1), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1,
    //                 shadowColor: "#000",
    //                 shadowOffset: { width: 2, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5,
    //             }}>


    //                 <TouchableOpacity
    //                     // underlayColor={false}
    //                     onPress={() => {
    //                         this.setState({
    //                             test: { name: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
    //                             pickerDisplayed: true
    //                         })
    //                     }}
    //                     style={styles.BlockNews}>

    //                     <View style={{ borderTopLeftRadius: 10, borderBottomLeftRadius: 10, overflow: 'hidden', }}>
    //                         <Image source={{ uri: item.image }} style={{ width: wp(45), height: wp(34), }} />
    //                     </View>

    //                     <View style={{ flexDirection: 'row', width: wp(93) }}>
    //                         <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: wp(3) }}>
    //                             <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('43'), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: wp('4%') }}>{item.name_news}</Text>
    //                             <View style={{ flexDirection: 'column', marginTop: wp(3) }}>
    //                                 <Text numberOfLines={3} style={{ color: "#000000AA", width: wp('42'), fontSize: wp(3), fontFamily: 'Prompt-Light', fontWeight: '400' }}>{item.detail}</Text>
    //                             </View>
    //                             {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: wp(4) }}>

    //               <Icon
    //                 // name={this.state.heartIcon}
    //                 onPress={() => { this.share() }}
    //                 name='ios-share-alt'
    //                 type='ionicon'
    //                 size={18}
    //                 containerStyle={{ justifyContent: 'center', marginTop: wp(1), marginLeft: wp(18) }}
    //                 color='black'
    //               />
    //               <Text style={{ marginLeft: wp(2), fontFamily: 'Prompt-Light', fontSize: 12, marginTop: wp(1) }} >แชร์</Text>


    //             </View> */}

    //                         </View>

    //                     </View>


    //                 </TouchableOpacity>
    //             </View>
    //         </View>
    //     );
    // }


    modaldetail = () =>
    
    
    
    {
        // alert(this.state.test.detail);
        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
        //let data = [1, 2, 3]
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });

 



        return (
            
            <LinearGradient colors={['#FFF', '#FFF']} style={styles.linearGradient}>
                <View style={{ flex: 1, flexDirection: 'column', }}>
                    <ScrollView style={{ flex: 1, }}
                        showsVerticalScrollIndicator={false}
                        style={styles.fill}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                        )}>

                        <View style={{ marginTop: wp(12), marginLeft: wp(4), }}>
                            <Ionicons
                                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                                name='ios-arrow-back'
                                color='black'
                                size={30}

                                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), fontWeight: '500' }}>ข่าวสารทั่วไป</Text>

                        </View>



                        <Text style={{ marginTop: wp(10), marginLeft: wp(3), marginRight: wp(3), width: wp(95), fontFamily: 'Prompt-SemiBold', fontSize: 18, color: 'black' }}>
                            {this.state.test.name}
                        </Text>

                        <View style={styles.cardNewsContainer}>
                            
                            <Carousel
                                ref={(c) => { this._carousel = c; }}
                                data={this.state.test.image_details}
                                sliderWidth={wp('100%')}
                                itemWidth={wp(70)}
                                enableSnap={true}
                                loop={true}
                                lockScrollWhileSnapping={true}
                                renderItem={({ item }) => {
                                    return (
                                        <View style={{ shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                                            <TouchableOpacity onPress={() => { this.setState({ zoomImage: true }) }}>
                                                <Image style={{ alignSelf: 'center', recideMode: 'contian', width: wp(70), height: wp(50), borderRadius: 10, marginTop: wp(5) }} source={{ uri: item.imagedetail }}
                                                    PlaceholderContent={<ActivityIndicator />} />
                                            </TouchableOpacity>

                                            <Modal visible={this.state.zoomImage} transparent={true} swipeDirection='down' swipeThreshold={50} onSwipeComplete={() => this.setState({ zoomImage: false })} >
                                                <ImageViewer imageUrls={this.state.test.image_details.map((item, index) => { return { url: item.imagedetail } })} />
                                                <Icon
                                                    underlayColor={false}
                                                    // raised
                                                    name='md-close'
                                                    type='ionicon'
                                                    color='#FFF'
                                                    size={wp(7)}
                                                    containerStyle={{ position: 'absolute', right: wp(10), top: wp(12), alignSelf: 'flex-end', marginRight: wp(-2) }}
                                                    onPress={() => this.setState({ zoomImage: false })}
                                                />
                                            </Modal>
                                        </View>
                                    );
                                }}
                            />
                        </View>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ marginTop: wp(5), marginLeft: wp(3.5), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black', fontWeight: '400', fontSize: wp(4) }}>
                                รายละเอียดโครงการ
                </Text>
                            <Text style={{ marginTop: wp(3), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>
                                {this.state.test.detail}
                            </Text>





                        </View>

                        <View style={styles.lineStylew2}></View>



                    </ScrollView>


                    {/* <Animated.View style={[styles.header, , { height: headerHeight }]}>
            <Animated.View>
              <View style={styles.bar}>
                <Text style={styles.title}  >TOT Easy Life</Text>
              </View>
            </Animated.View>

            <Animated.Image
              style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
              source={{ uri: this.state.test.image }} />


            <View style={{ marginTop: wp(-10), marginLeft: wp(4) }}>
              <Ionicons
                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                name='ios-arrow-back'
                color='white'
                size={45}

                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
            </View>
          </Animated.View> */}



                </View>
            </LinearGradient>
        )
    }


    render() {

        const Page1 = ({ label }) => (


            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flexDirection: 'column' }}>

                <View style={{ flex: 1, alignSelf: 'center' }}>


                    <FlatList
                        // showsHorizontalScrollIndicator={false}

                        data={this.state.ListNews}
                        ref={ref => (this.flatlist = ref)}
                        onScrollEndDrag={e => {
                            this.pagination(e.nativeEvent.velocity.x);
                        }}

                        renderItem={({ item }) => {
                            return (
                                
                                    <TouchableOpacity
                                        // underlayColor={false}
                                        // onPress={() => {
                                        //     this.setState({
                                        //         test: { name: item.title, detail: item.description, image: item.urlToImage, image_details: item.image_details },
                                        //         pickerDisplayed: true  
                                        //     })
                                        // }}
                                        
                                        onPress={() => this.props.navigation.navigate('WebNews', {url:item.url, title:'ข่าวอาสา'})} 
                                        // onPress={() =>  Linking.openURL(item.url)} 
                                        
                                        
                                        >

                                        <View style={{ alignSelf: 'center', marginBottom: wp(4), marginTop: wp('2%'), }} >
                                            <View style={{ width: wp(100), overflow: 'hidden', shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, }}>
                                                <Image source={{ uri: item.urlToImage }} style={{ width: wp('90%'), height: wp('45%'), alignSelf: 'center', borderRadius: 10, marginTop: wp(2), marginBottom: wp(2), }} />
                                            </View>

                                            <View style={{ flexDirection: 'row' }}>
                                                {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                                                <View style={{ flex: 1, flexDirection: 'column', }}>
                                                    <Text numberOfLines={2} style={{ alignSelf: 'center', color: "#000000EE", width: wp(90), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: wp('4%') }}>{item.title}</Text>
                                                    <View style={{ flexDirection: 'column', marginTop: 1 }}>
                                                        <Text numberOfLines={2} style={{ alignSelf: 'center', color: "#000000AA", width: wp(90), fontSize: wp('3.5%'), fontFamily: 'Prompt-Light' }}>{item.description}</Text>
                                                    </View>
                                                    {/* <View style={styles.lineStylew4} />

<Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                                                </View>
                                            </View>
                                        </View>

                                    </TouchableOpacity>
                             
                            )
                        }}
                    />

                </View>

            </ScrollView>
        );



        return (
            <View style={[styles.container, { paddingTop: wp(1) }]}>
                <View style={{ marginTop: wp(12) }}>

                    <Ionicons style={{ marginLeft: wp(0), alignSelf: 'flex-start', width: wp(90) }} name="ios-arrow-back" size={30} onPress={() => this.props.navigation.navigate('Home')} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(7), fontWeight: '500' }}>ข่าวสารทั่วไป</Text>
                </View>

                <Page1 tabLabel={{ label: "ข่าวสารอาสา" }} label="Page #1 " />
                {this.state.test != null ?
                    <Modal2

                        animationType={"slide"}
                        isVisible={this.state.pickerDisplayed}
                        onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
                        // swipeDirection={['down']}
                        style={styles.bottomModal}
                        onBackdropPress={() => this.setState({ pickerDisplayed: false })}

                    >

                        {
                            this.modaldetail()
                        }

                    </Modal2> : null
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        // marginTop:wp(20)
    },

    coverOverlayContainer: {
        backgroundColor: 'transparent',
        // backgroundColor: 'red',
        position: "absolute",
        top: wp(62),
        left: wp(0),
        right: 0,
        bottom: 0,
        width: wp(92),
        height: wp(15),
        justifyContent: 'space-between',
        // borderRadius: 10,
        resizeMode: "cover"
    },
    text: {
        color: '#000',
        fontFamily: 'Prompt-Light',
        fontSize: wp(4.2),
        fontWeight: '400'
        // marginTop: wp(-3)
    },

    logo: {
        backgroundColor: "white",
        height: HEIGHT,
        width: wp(92),
        marginTop: wp(5),
        // borderRadius: 10,

    },



    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
        flex: 1,

    },
    BlockNews: {
        flex: 1,
        backgroundColor: '#FFF',
        width: wp(93),
        borderRadius: 10,
        marginBottom: wp(3),
        marginTop: wp(2),
        height: wp(32.5),
        alignSelf: 'center',
        flexDirection: 'row',
    },
    linearGradient: {
        flexDirection: 'column',
        flex: 1,
        alignSelf: 'center',
        borderRadius: 5,
        height: 220,
        width: wp('100')
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000',
        overflow: 'hidden',
    },

    fill: {
        flex: 1,
    },
    bar: {
        marginTop: wp(8),
        height: wp(8),
        alignItems: 'center',
        justifyContent: 'center',


    },
    title: {
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: wp('4.5%'), fontWeight: '500',
        fontFamily: 'prompt-light'

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },


    cardNewsContainer: {
        alignSelf: 'center',
        marginTop: wp(5),
        shadowColor: "#000",
        backgroundColor: 'rgb(238, 238, 238)',
        width: wp(100), height: wp(60),

    },
    BlockNews1: {
        flex: 1,
        backgroundColor: '#FFF',
        margin: wp(1.5),
        borderRadius: 10,
        shadowColor: "#000",
        marginBottom: wp(3), marginTop: wp(3),
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
        alignSelf: 'center'
    },
    lineStylew1: {
        borderWidth: 0.25,
        borderColor: '#DCDCDC',
        alignSelf: 'center',
        width: wp(90),
        height: wp(0.5),
        marginTop: wp(20),
        backgroundColor: 'rgb(239, 239, 239)'
    },
    lineStylew2: {
        borderWidth: 0.25,
        borderColor: '#DCDCDC',
        alignSelf: 'center',
        width: wp(90),
        height: wp(0.5),
        marginTop: wp(5),
        backgroundColor: 'rgb(239, 239, 239)'
    },
});

